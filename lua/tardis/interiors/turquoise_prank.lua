-- 1965 TARDIS

local WINDOWS = "models/vtalanov98/turquoise_prank/windows"

local T = {
	ID = "turquoise_prank",
	Base = "tardis2010",
	Interior = {
		Screens = false,
		Light = { color = Color(0, 255, 230), warncolor = Color(255, 0, 0), pos = Vector(0, 0, 230), brightness = 5 },
		Lights = {
			{color = Color(0, 255, 230),	pos = Vector(0, 0, 35),		brightness = 1,	nopower = false, },
			{color = Color(0, 255, 200),	pos = Vector(0, 0, -60),	brightness = 1,	nopower = false, },
			{color = Color(0, 255, 230),	pos = Vector(0, 0, -60),				brightness = 0.6,	nopower = true, 	warncolor = Color(60, 0, 0),},
			{color = Color(0, 255, 230),	pos = Vector(0, 0, 40),					brightness = 0.2,	nopower = true, 	warncolor = Color(60, 0, 0),},
			{color = Color(0, 255, 230),	pos = Vector(337.3, -101.7, 58.4),		brightness = 0.5,	nopower = true, 	warncolor = Color(60, 0, 0),},
		},
		Parts = {
			turquoise_prank_hall = {
				pos = Vector(312.693, 350.175, 12.779),
				ang = Angle(-0.22401, -18.063, 93.466),
				matrixScale = Vector(1, 1.183, 2)
			},
			turquoise_prank_floor1 = {
				pos = Vector(282.393, 255.195, -36.005),
				ang = Angle(0, -18.59, 0),
			},
			turquoise_prank_floor2 = {
				pos = Vector(282.393, 255.195, -35.005),
				ang = Angle(0, -18.59, 0),
				matrixScale = Vector(0.35, 0.35, 1)
			},
			turquoise_prank_fakedoor = {
				pos = Vector(252.867, 168.739, 8.549),
				ang = Angle(90, -20, -90),
				scale = 1.7,
			},
			turquoise_prank_fakedoor2 = {
				pos = Vector(253.521, 166.689, 9.178),
				ang = Angle(90, -110.33, 0),
				scale = 1.1,
			},
		},
		CustomHooks = {
			prank_textures = {
				{
					["PostInitialize"] = true,
				},
				function(self)
					local intdoor = self:GetPart("door")
					if IsValid(intdoor) then
						intdoor:SetSubMaterial(4, WINDOWS)
						intdoor:SetSubMaterial(1, WINDOWS)
					end
				end,
			},
		},
	},
	Exterior = {
		CustomHooks = {
			prank_textures = {
				{
					["Initialize"] = true,
				},
				function(self)
					self:SetSubMaterial(3, WINDOWS)
					self:GetPart("door"):SetSubMaterial(2, WINDOWS)
				end,
			},
			prank_textures_dissolve = {
				{
					["PlayerEnter"] = true,
				},
				function(self, ply)
					if not self.interior or self.interior.turqprank_enter_time or self.interior.turqprank_dissolved then return end
					self.interior.turqprank_enter_time = CurTime()

					local screen = self.interior:GetPart("tardis2010_bigscanner")
					if IsValid(screen) then
						screen:SetMaterial("models/vtalanov98/turquoise_prank/rickroll")
					end

					local screen2 = self.interior:GetPart("tardis2010_monitor")
					if IsValid(screen2) then
						screen2:SetSubMaterial(1, "models/vtalanov98/turquoise_prank/rickroll")
					end
				end,
			},
		},
		ExcludedSkins = { 1, 2, 3, },
	},
}

if file.Exists("lua/tardis/interiors/turquoise.lua", "GAME") then
	T.Name = "Turquoise PRANK"
else
	T.Name = " Turquoise Glass Tardis v3 "
end

TARDIS:AddInterior(T)