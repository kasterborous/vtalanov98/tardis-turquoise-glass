function ThinkDissolve(self)
	if self.dissolved then return end
	if self.interior.turqprank_dissolved == true and not self.dissolved then
		self:SetColor(Color(0,0,0,0))
		self.dissolved = true
	end
	if self.interior.turqprank_dissolved ~= true and self.interior.turqprank_enter_time then
		local passed = CurTime() - self.interior.turqprank_enter_time
		self:SetRenderMode(RENDERMODE_TRANSALPHA)
		self:SetColor(Color(255,255,255, 255 * math.max(0, 1 - (passed / 3.5) )))
		if passed > 3.5 then
			self.interior.turqprank_dissolved = true

			local intdoor = self.interior:GetPart("door")
			if IsValid(intdoor) then
				intdoor:SetSubMaterial(1)
				intdoor:SetSubMaterial(4)
			end

			self.exterior:SetSubMaterial(3)
			local extdoor = self.exterior:GetPart("door")
			if IsValid(extdoor) then
				extdoor:SetSubMaterial(2)
			end
		end
	end
end


local PART={}
PART.ID = "turquoise_prank_hall"
PART.Name = "Turquoise Prank TARDIS coridor"
PART.Model = "models/props_phx/construct/metal_plate_curve360x2.mdl"
PART.AutoSetup = true
PART.Collision = false
PART.Animate = false

function PART:Initialize()
	self:SetMaterial("models/vtalanov98/turquoise_prank/bluepanel")
	self:SetHealth(1)
end

PART.Think = function(self)
	ThinkDissolve(self)
end

TARDIS:AddPart(PART)

local PART={}
PART.ID = "turquoise_prank_floor1"
PART.Name = "Turquoise Prank TARDIS floor 1"
PART.Model = "models/hunter/plates/plate2x4.mdl"
PART.AutoSetup = true
PART.Collision = false
PART.Animate = false

function PART:Initialize()
	self:SetMaterial("models/doctorwho1200/copper/bluepanel")
end
PART.Think = function(self)
	ThinkDissolve(self)
end

TARDIS:AddPart(PART)

PART.ID = "turquoise_prank_floor2"
PART.Name = "Turquoise Prank TARDIS floor 2"
PART.Model = "models/hunter/plates/plate2x16.mdl"

function PART:Initialize()
	self:SetMaterial("models/doctorwho1200/copper/grate")
end

TARDIS:AddPart(PART)

local PART={}
PART.ID = "turquoise_prank_fakedoor"
PART.Name = "Turquoise Prank TARDIS fake door"
PART.Model = "models/hunter/plates/plate1x1.mdl"
PART.AutoSetup = true
PART.Collision = false
PART.Animate = false

function PART:Initialize()
	self:SetMaterial("models/vtalanov98/turquoise_prank/fakedoor")
end
PART.Think = function(self)
	ThinkDissolve(self)
end

TARDIS:AddPart(PART)

local PART={}
PART.ID = "turquoise_prank_fakedoor2"
PART.Name = "Turquoise Prank TARDIS fake door 2"
PART.Model = "models/hunter/plates/plate2x2.mdl"
PART.AutoSetup = true
PART.Collision = false
PART.Animate = false

function PART:Initialize()
	self:SetMaterial("models/doctorwho1200/copper/black")
end
PART.Think = function(self)
	ThinkDissolve(self)
end

TARDIS:AddPart(PART)